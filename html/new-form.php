<?php declare(strict_types = 1);

namespace HuonCS\FormBuilderApp;
require_once 'init.php';

use HuonCS\FormBuilder\Database;


$database = new Database('FormBuilderApp');

$formcheck = @$_POST['form'];
$formfile = @$_POST['formfile'] ?: @$_POST['basename'];
$source = @$_POST['source'];

if ($formcheck == 'new-form' && preg_match('/^[-_a-zA-Z0-9]+$/',$formfile) && trim($source)) {
    $database->saveForm($formfile, $source);
}

header('Location: /');
