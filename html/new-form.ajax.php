<?php declare(strict_types = 1);

namespace HuonCS\FormBuilderApp;

require_once 'init.php';

use HuonCS\FormBuilder\Database;

$database = new Database('FormBuilderApp');

switch (@$_POST['command']) {
    case 'getFormFiles':
        $result = ['ok' => true, 'forms' => $database->getFormNames()];
        break;
    case 'readFormFile':
        $form = @$_POST['form'];
        if (preg_match('/^[-_a-z0-9]+$/',$form) && $database->formExists($form)) {
            $result = ['ok' => true, 'source' => $database->getSource($form)];
        } else {
            $result = ['ok' => false, 'error' => 'Form not found.'];
        }
        break;
    default:
        $result = ['ok' => false, 'error' => 'Unknown command.'];
}

header('Content-type: application/json');
print json_encode($result);
