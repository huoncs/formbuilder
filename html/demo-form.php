<?php

try {
	$formname = @$_GET['form'];
	if (!preg_match('/^[-_a-zA-Z0-9]+$/',$formname)) throw new Exception('Form name is not valid: identifier limited to alphanumeric, dash or underscore.');
	if (!file_exists("forms/{$formname}.form")) throw new Exception("Form {$formname}.form not found");

	$html = file_get_contents('demo-form.html');
	$html = preg_replace('/<<<FORMNAME>>>/',$formname,$html);
	
	if ($_POST) {
		$table = '<table class="result"><thead><tr><th>Field</th><th>Value</th></tr></thead><tbody>';
		foreach ($_POST as $field => $value) {
			$table .= '<tr><th>'.htmlentities($field).'</th><td>'.htmlentities($value).'</td></tr>';
		}
		$table .= '</tbody></table>';
	} else {
		$table = '';
	}
	$html = preg_replace('/<<<POST>>>/',$table,$html);
	print $html;
	exit(0);
}
catch (Exception $e) {
	print "<h3>Error</h3>";
	print "<p>{$e->getMessage()}</p>";
}